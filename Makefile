# app name, the single edit point for the whole Makefile
APP := ejose

DESTDIR ?= install-dir

VERSION := $(shell cat version)

# this scary thing returns random word from dictionary for release name
ROLLDICE := $(shell perl -e 'open F,"</usr/share/dict/american-english"or exit 1;@l=<F>;do{$$r=rand scalar @l}until($$l[$$r]=~s/([A-Z][a-z]*).*/$$1/&&$$l[$$r]=~y===c>3);print $$l[$$r]' || echo no-dices)

# current release name from the file
RELEASE := $(shell cat release)

DATETIME := $(shell LC_ALL=C date)
HOSTNAME := $(shell hostname)

# list of erlang sources and beams to make
ERL_SRC := $(wildcard src/*.erl)
BEAMS   := $(patsubst src/%.erl, ebin/%.beam, $(ERL_SRC))

.PHONY: all compile html clean eunit dialyze all-tests

all: compile

ERLC_OPTS := +verbose \
        +warn_unused_function \
        +warn_bif_clash \
        +warn_deprecated_function \
        +warn_obsolete_guard \
        +warn_shadow_vars \
        +warn_export_vars \
        +warn_unused_records \
        +warn_unused_import \
        +warn_export_all \
        -Dejose_version=\"$(VERSION)\" \
        -Dejose_release=\"$(RELEASE)\" \
        -Werror

ifdef DEBUG
ERLC_OPTS := $(ERLC_OPTS), debug_info
endif

ifdef TEST
ERLC_OPTS := $(ERLC_OPTS), {d, 'TEST'}
endif

compile: ebin/$(APP).app $(BEAMS)

ebin/$(APP).app: src/$(APP).app.in
	mkdir -p ebin
	sed "s/{{VERSION}}/$(VERSION)/" src/$(APP).app.in > ebin/$(APP).app

ebin/%.beam: src/%.erl
	erlc -o ebin -I include $(ERLC_OPTS) $<

docs: edoc

edoc: doc doc/overview.edoc doc/index.html

doc:
	mkdir -p doc

EDOC_OPTS = {application, $(APP)}, {preprocess, true}
doc/index.html: doc/overview.edoc
	erl -noinput -eval 'edoc:application($(APP),".",[$(EDOC_OPTS)]),halt()'

doc/overview.edoc: src/overview.edoc.in
	sed "s/{{VERSION}}/$(VERSION)/" src/overview.edoc.in > doc/overview.edoc

test/ejose_test.beam: test/ejose_test.erl
	erlc -o test $<

eunit: clean compile test/ejose_test.beam
	erl -noinput -pa ebin -pa test \
		-eval 'ok=eunit:test($(APP)_test,[verbose]),halt()'

PLT = .dialyzer_plt
DIALYZER_OPTS = -Wunmatched_returns -Werror_handling
DIALYZER_APPS = erts inets kernel stdlib crypto compiler \
	envx_config envx_logger envx_df envx_lib envx_ssp_resolver \
	envx_commons cowlib cowboy ranch

dialyze: $(PLT)
	$(MAKE) DEBUG=y clean compile
	dialyzer --plt $< -r . $(DIALYZER_OPTS) --src
	dialyzer --plt $< -r . $(DIALYZER_OPTS)

$(PLT):
	dialyzer --build_plt --output_plt $@ --apps $(DIALYZER_APPS)

CONFIG=`[ -f $(APP).config ] && echo "-config $(APP).config" ||:`
shell: compile
	erl -pa ebin $(CONFIG) \
		-eval 'application:ensure_all_started($(APP))'

test: eunit

all-tests:
	$(MAKE) eunit
	$(MAKE) dialyze

DISTR := --distribution unstable

# update debian/changelog with all the commits in new version
.PHONY: debian/changelog
debian/changelog:
	git log --oneline $(VERSION).. |\
		while read -r line; do dch -v$(NEWVER) "$$line"; done
	dch $(DISTR) -v $(NEWVER) "New release: $(NEWVER) $(ROLLDICE)"

# make changes to the version files and then tag it and commit it
newtag: askfirst debian/changelog
	echo "$(ROLLDICE)" > release
	echo "$(NEWVER)" > version
	git commit -am "release: $(NEWVER) $(ROLLDICE)"
	git tag -m"New release: $(ROLLDICE)" $(NEWVER) HEAD

# helper target to ask if we really sure to do what we are going to do
askfirst:
	@echo "$(VERSION) -> $(NEWVER), codename: $(ROLLDICE)"
	@echo "changes in git:"
	@git log --oneline $(VERSION).. |\
		while read -r line; do echo "  * $$line"; done
	@echo "are you sure?"
	@read yn; if [ "_$$yn" != "_y" ]; then echo "abort."; exit 2; fi
	@echo "ok, proceeding"

rel: NEWVER = $(shell echo $(VERSION)| perl -e '@a=split /\./,<>;print(($$a[0]+1).".0.0")')
rel: newtag

# mark new release and push it to the upstream server
release: NEWVER=$(shell perl -e '@a=split /\./,"$(VERSION)";print "$$a[0].$$a[1].".($$a[2]+1)')
release: newtag push

# the same as release, but changing minor version digit
newmin: NEWVER = $(shell perl -e '@a=split /\./,"$(VERSION)";print "$$a[0].".($$a[1]+1).".0"')
newmin: newtag push

# the same as release, but change major version digit
newmaj: NEWVER = $(shell echo $(VERSION)| perl -e '@a=split /\./,<>;print(($$a[0]+1).".0.0")')
newmaj: newtag push

# push commits and tags to the upstream server
.PHONY: push
push:
	git push
	git push --tags

clean:
	rm -rf ebin doc
	rm -f erl_crash.dump Emakefile *.log *.log.* tmp_file test/*.beam
	find . -type f -name '*~' -delete

