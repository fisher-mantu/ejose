# README #

erlang library for JWT & JWE

### What is this repository for? ###

* Quick summary

Reduced set functionality for creating and operating on JSON Web Token and Encryption

### How do I get set up? ###

* Dependencies
    + jiffy for JSON ops (git@github.com:davisp/jiffy.git)
    + base64, rand (part of erlang's stdlib)
    + crypto (usually shipped with erlang base)
    + eunit (for unit tests only)
* How to compile it
    + ensure you have jiffy installed and then just
    > $ make
    + I use GNU make and erlc command line Erlang comiler
* How to run tests
    + ensure you have eunit module installed and then
    > $ make && make test
* Deployment instructions
> compile it and install ebin directory somewhere
> in your $ERL_LIBS or even code:root_dir().
> Erlang VM on start searches for code modules through
> the {root_dir}/lib/*/ebin and then $ERL_LIBS/*/ebin
> So you can create {root_dir}/lib/ejose/ebin and put there
> compiled .beam files with .app file from the ebin directory
> and voila

### Whom should I talk to?

* username serge.fisher ath Gmail dod com

