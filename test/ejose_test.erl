-module(ejose_test).

-include_lib("eunit/include/eunit.hrl").

-export([start/0]).

%% for debug purposes only
%% -export([jwt_256_1/0, simple_payload/0]).

%% -compile([export_all]).

%% a number, a string and a boolean
simple_payload() ->
    #{<<"sub">> => 12345, <<"name">> => <<"John Doe">>, <<"admin">> => true}.

%% HS256, {sub:12345, name:"John Doe", admin:true}
jwt_256_0() ->
    <<"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMzQ1LCJuYW1lIjoiSm9obiBEb2UiLCJhZG1pbiI6dHJ1ZX0.CsaPYz0dVITT2BOnvddOnQROxvO-O-0qAv_8nFzqvSc">>.

%% the same as above but from external 3rd party lib
jwt_256_1() ->
    <<"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMzQ1LCJuYW1lIjoiSm9obiBEb2UiLCJhZG1pbiI6dHJ1ZX0.CsaPYz0dVITT2BOnvddOnQROxvO-O-0qAv_8nFzqvSc">>.

%% the same but signature applied to non-stripped mime_base64
jwt_256_2() ->
    <<"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMzQ1LCJuYW1lIjoiSm9obiBEb2UiLCJhZG1pbiI6dHJ1ZX0=.D4ioYQyy5M3Yz57eNDP4IWn4mXhP4GD7MPzHioghlMg=">>.

%% the same but using hs512 (mime base64)
jwt_512_0() ->
    <<"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOjEyMzQ1LCJuYW1lIjoiSm9obiBEb2UiLCJhZG1pbiI6dHJ1ZX0./uPswakAuZzIeMnwa9d8euWzn1iIOMRqkO+y3Hld0vAUMiEDE5alb+HDnrodsN04+g/TR/7XMvOHv0IoAiTc1Q==">>.

%% the same but using hs512 (URL base64)
jwt_512_1() ->
    <<"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOjEyMzQ1LCJuYW1lIjoiSm9obiBEb2UiLCJhZG1pbiI6dHJ1ZX0._uPswakAuZzIeMnwa9d8euWzn1iIOMRqkO-y3Hld0vAUMiEDE5alb-HDnrodsN04-g_TR/7XMvOHv0IoAiTc1Q">>.

%% ----------------------------------------------------------------------

start() ->
    not_implemented_yet.

jwt_cross_test_() ->
    RndFloat = randomfloat(),
    Payload = #{ <<"iss">> => 123 },
    Payload1 = #{ <<"qwe">> => <<"asdf">>, <<"zxc">> => randomfloat(),
                  <<"ghj">> => true, <<"uio">> => rand:uniform(1235),
                  <<"hdj">> => false, <<"ijb">> => [null, 123] },
    [
     ?_assertEqual( {ok, Payload},
                    ejose_jwt:verify(
                      ejose_jwt:create( Payload, <<"asdf">>, hs256),
                      <<"asdf">>) ),
     ?_assertEqual( {ok, #{<<"qaz">> => RndFloat}},
                     ejose_jwt:verify(
                       ejose_jwt:create( #{<<"qaz">> => RndFloat},
                                         <<"qwer">>, hs512),
                       <<"qwer">>) ),
     ?_assertEqual( {ok, Payload1},
                     ejose_jwt:verify(
                       ejose_jwt:create( Payload1, <<"qwer">>, hs512),
                       <<"qwer">>) )
    ].

jwt_decoding_test_() ->
    [
     ?_assertEqual( {ok, simple_payload()},
                    ejose_jwt:verify(jwt_256_0(), <<"secret">>)),
     ?_assertEqual( {ok, simple_payload()},
                    ejose_jwt:verify(jwt_256_1(), <<"secret">>)),
     ?_assertEqual( {ok, simple_payload()},
                    ejose_jwt:verify(jwt_256_2(), <<"secret">>)),
     ?_assertEqual( {ok, simple_payload()},
                    ejose_jwt:verify(jwt_512_0(), <<"secret">>)),
     ?_assertEqual( {ok, simple_payload()},
                    ejose_jwt:verify(jwt_512_1(), <<"secret">>))
    ].

jwt_encoding_test_() ->
    [
     ?_assertEqual( jwt_256_0(),
                    ejose_jwt:create( simple_payload(), <<"secret">>, hs256) ),
     ?_assertEqual( jwt_256_0(),
                    ejose_jwt:create(
                      #{<<"sub">> => 12345, <<"admin">> => true,
                        <<"name">> => <<"John Doe">>},
                      <<"secret">>, hs256) ),
     ?_assertEqual( jwt_256_0(),
                    ejose_jwt:create(
                      #{<<"name">> => <<"John Doe">>, <<"sub">> => 12345,
                        <<"admin">> => true}, % different order payload
                      <<"secret">>, hs256) )
    ].

jwe_cross_test_() ->
    PlainText = <<"just a test string">>,
    PreSharedKey256 = <<"01234567012345670123456701234567">>,
    RandomBinaryPayload = crypto:strong_rand_bytes(rand:uniform(128)),
    RandomPreSharedKey = crypto:strong_rand_bytes(32),
    [
     ?_assertEqual( PlainText,
                   ejose_jwe:decrypt(
                     ejose_jwe:encrypt( PlainText, PreSharedKey256 ),
                     PreSharedKey256)),
     ?_assertEqual( RandomBinaryPayload,
                   ejose_jwe:decrypt(
                     ejose_jwe:encrypt( RandomBinaryPayload, PreSharedKey256 ),
                     PreSharedKey256)),
     ?_assertEqual( RandomBinaryPayload,
                   ejose_jwe:decrypt(
                     ejose_jwe:encrypt( RandomBinaryPayload, RandomPreSharedKey ),
                     RandomPreSharedKey))
    ].

%% test for correct error reporting on invalid request
invalid_req_test_() ->
    [
     ?_assertEqual( {error, invalid_signature},
                    ejose_jwt:verify(jwt_256_1(),
                                     <<"Secret">>)), % should be lowercase
     ?_assertEqual( {error, {wrong_jwt, 1234}}, ejose_jwt:verify(1234, <<>>))
    ].


%% ----------------------------------------------------------------------
%% internal helper funs

%% 64bit long (double precission)
randomfloat() ->
    <<F/float>> = crypto:strong_rand_bytes(8),
    F.
